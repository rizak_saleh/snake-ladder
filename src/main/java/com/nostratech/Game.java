package com.nostratech;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter number of the Snakes:");
        int numOfSnakes = scanner.nextInt();
        List<Snake> snakes = new ArrayList<Snake>();
        System.out.println("Enter the tail and head position:");
        for (int i = 0; i < numOfSnakes; i++) {
            snakes.add(new Snake(scanner.nextInt(), scanner.nextInt()));
        }

        System.out.println("Enter number of the Ladders:");
        int numOfLadders = scanner.nextInt();
        List<Ladder> ladders = new ArrayList<Ladder>();
        System.out.println("Enter start and end position:");
        for (int i = 0; i < numOfLadders; i++) {
            ladders.add(new Ladder(scanner.nextInt(), scanner.nextInt()));
        }

        System.out.println("Enter number of the Players:");
        int numOfPlayers = scanner.nextInt();
        List<Player> players = new ArrayList<Player>();
        System.out.println("Enter name of player:");
        for (int i = 0; i < numOfPlayers; i++) {
            players.add(new Player(scanner.next()));
        }

        Plotting plotting = new Plotting();
        plotting.setPlayers(players);
        plotting.setSnakes(snakes);
        plotting.setLadders(ladders);

        plotting.startGame();

	}

}
