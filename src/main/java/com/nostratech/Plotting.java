package com.nostratech;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;


public class Plotting {
	 private Board board;
	    private int initialNumberOfPlayers;
	    private Queue<Player> players; 

	    private static final int BOARD_SIZE = 100; //jumlah cell 100.

	    public Plotting(int boardSize) {
	        this.board = new Board(boardSize);
	        this.players = new LinkedList<Player>();
	    }

	    public Plotting() {
	        this(Plotting.BOARD_SIZE);
	    }

	    public void setPlayers(List<Player> players) {
	        this.players = new LinkedList<Player>();
	        this.initialNumberOfPlayers = players.size();
	        Map<String, Integer> partOfPlayer = new HashMap<String, Integer>();
	        for (Player player : players) {
	            this.players.add(player);
	            partOfPlayer.put(player.getId(), 0); //Inisialisasi posisi awal setiap pemain, yaitu 0 (posisi di luar board)
	        }
	        board.setPartPlayer(partOfPlayer); //  tambahkan pemain board
	    }

	    public void setSnakes(List<Snake> snakes) {
	        board.setSnakes(snakes); // tambahkan ular ke board
	    }

	    public void setLadders(List<Ladder> ladders) {
	        board.setLadders(ladders); // tambahkan ular tangga board
	    }

	    private int getNewPosition(int newPosition) {
	        int previousPosition;

	        do {
	            previousPosition = newPosition;
	         // ketika ketemu ekor ular
	            for (Snake snake : board.getSnakes()) {
	                if (snake.getStart() == newPosition) {
	                    newPosition = snake.getEnd(); 
	                    System.out.println("Sorry You get down to " + newPosition);
	                }
	            }
	            
	         // ketika ketemu tangga
	            for (Ladder ladder : board.getLadders()) {
	                if (ladder.getStart() == newPosition) {
	                    newPosition = ladder.getEnd(); 
	                    System.out.println("Horee!!! You are going up to " + newPosition);
	                }
	                
	            }
	        } while (newPosition != previousPosition);
	        return newPosition;
	    }

	    private void movePlayer(Player player, int positions) {
	        int oldPosition = board.getPartPlayer().get(player.getId());
	        int newPosition = oldPosition + positions; // moving

	        int boardSize = board.getSize();

	        //ketika mendapat dadu yang dapat membuat pemain keluar board, maka pemain tidak akan bergerak/tetap pada posisi saat itu
	        if (newPosition > boardSize) {
	            newPosition = oldPosition; 
	        } else {
	            newPosition = getNewPosition(newPosition);
	        }

	        board.getPartPlayer().put(player.getId(), newPosition);

	        System.out.println(player.getName() + " rolled a " + positions + " and moved from " + oldPosition +" to " + newPosition);
	    }

	    private int getDiceValue() {
	        return Dice.roll();
	    }

	    private boolean hasPlayerWon(Player player) {
	        int playerPosition = board.getPartPlayer().get(player.getId());
	        int winningPosition = board.getSize();
	        return playerPosition == winningPosition; 
	    }

	    private boolean isGameCompleted() {
	        int currentNumberOfPlayers = players.size();
	        return currentNumberOfPlayers < initialNumberOfPlayers;
	    }

	    public void startGame() {
	        while (!isGameCompleted()) {
	        	// lempar dadu setiap giliran main
	            int totalDiceValue = getDiceValue(); 
	            Player currentPlayer = players.poll();
	            movePlayer(currentPlayer, totalDiceValue);
	            if (hasPlayerWon(currentPlayer)) {
	                System.out.println("CONGRATULATION!!!  " + currentPlayer.getName() + " WINS THE GAME");
	                board.getPartPlayer().remove(currentPlayer.getId());
	            } else {
	                players.add(currentPlayer);
	            }
	        }
	    }


}
