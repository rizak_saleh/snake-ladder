package com.nostratech;

public class Snake {
	private int tail;
    private int head;

    public Snake(int tail, int head) {
        this.tail = tail;
        this.head = head;
    }

    public int getStart() {
        return tail;
    }

    public int getEnd() {
        return head;
    }
}
